// slots.cpp
// Due 2/8/16
// Author: Ronni Sardina
// Derived Class of casino

//#include "casino.h"
#include "slots.h"

using namespace std;
//Symbols for the class: 
// default constructor for the slots class
slots::slots() : casino() {
	currentMoney = 77777;
	initialMoney = 77777;
	userName = "USERNAME";
	gameName = "Slots";
	gameBet = 10;
	gameMoney = 0;
	wheel1 = ' ';
	wheel2 = ' ';
	wheel3 = ' ';
	wheel4 = ' ';
	play = 1; 
	set<char> icons;
	matches = 0;
}

// derived class function calls
void slots::print() {
	cout<<"\t/TTTTTTTTTTTTTTT\\"<<endl;
	cout<<"\t|_______________| /==O\t\tEarnings: $"<<gameMoney<<endl;
 	cout<<"\t| "<<wheel1<<" | "<<wheel2<<" | "<<wheel3<<" | "<<wheel4<<" | ||"<< endl;
	cout<<"\t|   |   |   |   | ||\t\tPlay Again? ($10)    Yes (1)  No(0)"<<endl;
	cout<<"\t|_______________| ||"<<endl;
	}
void slots::playGame() {
	cout<<"Welcome to slots! The cost to play is $10. Here are the rules:\n";
	cout<<"  2 icons match: return of $15 (gain of $5)\n";
	cout<<"  3 icons match: return of $40 (gain of $30)\n";
	cout<<"  4 icons match: return of $510 (gain of $500)\n"<<endl;
	
	while (play) {
		wheel1 = spin();
		wheel2 = spin();
		wheel3 = spin();
		wheel4 = spin();
	
		//print();
		int n = compare();
		//cout<<"COMPARE RETURNS: "<<n<<endl;
		matches = n;
		switch (matches) {
			case 4: 	//0matches
				cout<<"\t0 MATCHES: -$10"<<endl;
				gameMoney = gameMoney-10;
				break;
			case 3:	//2
				cout<<"\t2 ICONS MATCH: +$5"<<endl;
				gameMoney = gameMoney+5;
				break;
			case 2:	//3
				cout<<"\t3 ICONS MATCH: +$30"<<endl;
				gameMoney = gameMoney+30;
				break;
			case 1:	//4
				cout<<"\t4 ICONS MATCH: +$500"<<endl;
				gameMoney = gameMoney+500;
				break;	
		}
		print();
		//cout<<"Earnings: "<<gameMoney<<endl;
		//cout<<"Play Again? Yes=1 No = 0: ";
		cin>>play;
	}
}
char slots::spin() {
	int num = rand() % 9;
	return images[num];
}
int slots::compare() {
	
	iconCount.clear();
	iconCount[wheel1]++;
	iconCount[wheel2]++;
	iconCount[wheel3]++;
	iconCount[wheel4]++;
	
	int numEl = iconCount.size();
	if (numEl == 2)	{	//accounts for possibility of two pairs or three of a kind
		if (iconCount[wheel1] > 2 || iconCount[wheel2] > 2 || iconCount[wheel3] > 2){
			return 2;		//triple
		}
		else {
			return 3;		//twopairs
		}
	}
	
	return numEl;
}
