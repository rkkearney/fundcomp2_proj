// blackjack.h (class definition)
// Due 2/8/16
// Author: Reilly Kearney and Taylor Rongaus
// Derived class of casino

#ifndef blackjack_H
#define blackjack_H
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <deque>
#include <algorithm>
#include <stdlib.h>
#include "casino.h"
using namespace std;

// Derived blackjack class; inherits from casino
class blackjack : public casino {
	
	public:

		blackjack(); 						// default constructor for blackjack class
		blackjack( double, int = 52 );
		void initializeDeck();
		void initPrint();
		void shuffle();						// shuffles cards
		int getSize();						// returns size of deck
		void playBlackJack();				// BlackJack Interface
		int placeBet( double );				// place a bet at beginning of each game
		void Payout( int );					// payout if win or push
	
	private:

		int Deal();							// deals cards
		int playAgain();					// asks user if still playing
		int checkWinner();					// determines winner (0 dealer, 1 player, -1 push) & prints appropriate message
		int checkHit();						// returns 0 if stand, 1 if hit
		int checkCardValue( int );			// adjust card value for ace, king, queen, and jack
		void printCard( int );				// prints card
		int check21( int, char );			// checks for BlackJack
		double netChange();					// returns net money lost or gained

		/* Cards */
		struct Card {
			int suitRank;
			int valueRank;
			int number;
			char suit;
			char value;
		};
		deque< Card > deck;

		/* Player */
		struct Player {
			Card *hand;				// player hand
			int handScore;
			string handType;		// for printing
			double currentBet;		// bet for that round
			double totalMoney;		// total money
		};

		Player player;
		Player dealer;

		double startMoney;					// different from initialMoney
		/* Information from base class - will adjust in future to link*/
		double currentMoney;

};

#endif
