// holdem.h (class definition)
// Due 2/8/16
// Author: Taylor Rongaus
// Derived class of casino

#ifndef holdem_H
#define holdem_H
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include "math.h"
#include "casino.h"
using namespace std;

// Derived holdem class; inherits from casino
class holdem : public casino {
	public:
		holdem();	// default constructor for holdem class
		void initPrint();
	private:

};

#endif
