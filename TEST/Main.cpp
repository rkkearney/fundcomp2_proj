#include "casino.h"
#include "roulette.h"
//#include "blackjack.h"
#include "fivecardstud.h"
#include "slots.h"
#include "keno.h"
#include <iomanip>

using namespace std;

int main() {
	// ensure random shuffling
	srand(time(NULL));
	
	// read in username and determine if they've visited the casino before
	string username;
	cout << "Welcome to the Casino! What is your name? ";
	cin >> username;
	int match = 0;
	string line;
	double startMoney = 500;
	ifstream myfile ("leaderboard.txt");
	// read in leaderboard file to determine if the user has played before or not
	if (myfile.is_open()) {
		while (getline(myfile,line)) {
			if (match == 1) {
				startMoney = atof(line.c_str());
				myfile.close();
			}
			if (line == username) {
				match = 1;
			}
		}
	}
	// instantiate casino object
	casino C(username, startMoney);
	// if the match variable != 0, they have been here before
	if (match != 0) {
		cout << "Good to have you back, " << username << "! It looks like you still have $" << C.getCurrentWallet() << " left." << endl;
	}
	else {
		cout << "It looks like you're new to the Casino, " << username << ", so we'll start you off with $" << C.getCurrentWallet() << endl;
	}
	
	// call the wordsPrint functions of the base class
	int gameChoice = 0;
	C.wordsPrint("welcome2.txt");
	
	// instantiate objects of derived classes
	roulette R(username,C.getCurrentWallet());
	//blackjack B(500,52);
	FiveCard F(username,C.getCurrentWallet());
	slots S(username,C.getCurrentWallet());
	keno K(username,C.getCurrentWallet());
	
	while (gameChoice != 6) {
		if ((gameChoice >= 1) && (gameChoice <= 5)) {
			C.wordsPrint("welcome1.txt");
		}
		// THIS SHOULD GIVE THE VALUE UPDATED IN KENO WTF IT WORKS THERE
		cout << "You still have $"<< C.getCurrentWallet() << " in your wallet." << endl;
		gameChoice = C.print();
		cout << endl;
		switch (gameChoice) {
			case 1:
				R.initPrint();
				C.setCurrentWallet(R.playRoulette(C.getCurrentWallet()));
			break;
			case 2:
				cout << "blackjack entered" << endl;
			//	B.initPrint();
			//	B.playBlackJack();
			break;
			case 3:
				F.initPrint();
				//F.playFiveCard(C.getCurrentWallet());
				F.playFiveCard();
			break;
			case 4:
				S.initPrint();
				S.playSlots(C.getCurrentWallet());
			break;
			case 5:
				K.initPrint();
				C.setCurrentWallet(K.playKeno(C.getCurrentWallet()));
			break;
			case 6:
				C.wordsPrint("thanks.txt");
				// use value of match to determine whether to append or overwrite
				// if no match, append new user to end of leaderboard list
				if (match == 0) {
					ofstream myfile;
					myfile.open("leaderboard.txt", ios_base::app);
					myfile << C.getUsername() << endl;
					myfile << C.getCurrentWallet() << endl;
					myfile.close();
				}
				// if a match, overwrite previous data
				else {
					ifstream myfile;
					myfile.open("leaderboard.txt");
					string line;
					if (myfile.is_open()) {		
						while (getline(myfile,line)) {
							if (match == 2) {
								// NOT WORKING, CAN'T JUST DELETE A LINE
								// need to re-write the entire file & replace the line w/ new str
								//cout << "entered match = 2" << endl;
								//cout << C.getCurrentWallet() << endl;
								ofstream myfile;
								myfile << C.getCurrentWallet() << endl;
							}
							if (line == C.getUsername()) {
								match = 2;
							}
						}
					}
					else {
						cout << "Error: Unable to open file." << endl;
					}
					myfile.close();
				}
			break;
			default:
				cout << "Error: please enter a valid game choice. " << endl;
			break;
		}
	}
}

