// fivecardstud.h
// Author: Reilly Kearney
// Derived class of casino

#ifndef fivecardstud_h
#define fivecardstud_h

#include <iostream>
#include <string>
#include <deque>
#include <iomanip>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include "casino.h"
using namespace std;

class FiveCard : public casino {
	public:
		FiveCard( string, double );
		void initializeGame();
		void initializeDeck();
		void shuffle();
		void playFiveCard();			// gameplay Interface
		void initPrint();
		void EndOfGame();
	private:
		void deal();					// deals cards
		void draw();					// prompts and draws cards
		void opponentDraw();			// draw cards for opponent
		void ante();					// initial bets
		int betPrompt( int );
		int opponentBet( int );
		int playAgain();				// asks user if still playing
		int checkWinner( int, int );
		void calculateHandScore();
		int checkFourofaKind( int );
		int checkFullHouse( int );
		int checkFlush( int );
		int checkStraight( int );
		int checkThreeofaKind( int );
		int checkTwoPair( int );
		int checkOnePair( int );
		int checkHighCard( int );
		void reset();					// resets for next round
		void printHand( int );			// prints card

		/* Cards */
		struct Card {
			int suitRank;
			int valueRank;
			int number;
			char suit;
			char value;
		};
		deque< Card > deck;
		
		double pot;

		/* Player */
		struct Player {
			Card *hand;				// player hand
			int handScore;
			string handType;		// for printing
			double totalBet;
			double currentBet;		// bet for that round
			double totalMoney;		// total money
		};

		Player player;
		Player opponent;
};

#endif
