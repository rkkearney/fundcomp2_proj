// keno.h (class definition)
// Due 2/8/16
// Author: Maddie Gleason and Taylor Rongaus
// Derived class of casino

#ifndef keno_H
#define keno_H
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <cstring>
#include "math.h"
#include "casino.h"
using namespace std;

// Derived keno class; inherits from casino
class keno : public casino {
	public:
		keno(string, double);						// default constructor 
		void playerChoice();				// have player enter 10 choices
		int checkChoice();					// compare player choices to game numbers
		double playKeno(double);					// game play function 
		void initPrint();
	private:
		int userNumbers[10];	// user entered array of 10 numbers
		int gameNumbers[20];	// game array of 20 random numbers
		int playAgain();		// checks if user wants to play again
		void kenoArray();		// initializes 20 numbered array
};

#endif
