// casino.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Base Class

#include "casino.h"

using namespace std;

// default constructor
casino::casino(string u, double cw) {
	username = u;
	currentWallet = cw;
}

void casino::wordsPrint(string file) {
	// print the file to stdout
	string tmp;
	ifstream myFile (file.c_str());
	cout << endl;
	if (myFile.is_open()) {
		while(myFile.good()) {
			getline(myFile, tmp);
			cout << tmp << endl;
		}
	}
}

int casino::print() {
	int choice;
	cout << "What game would you like to play?" << endl;
	cout << "1) Roulette " << endl;
	cout << "2) Blackjack " << endl;
	cout << "3) Five-Card Stud " << endl;
	cout << "4) Slots " << endl;
	cout << "5) Keno " << endl;
	cout << "6) Quit " << endl;
	cout << "Your choice: ";
	cin >> choice;
	return choice;
}

string casino::getUsername() {
	return username;
}

void casino::setUsername(string s) {
	username = s;
}

double casino::getCurrentWallet() {
	return currentWallet;
}

void casino::setCurrentWallet(double cw) {
	currentWallet = cw;
}

