#ifndef KENO_H 
#define KENO_H

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;

class keno {
	
	public:
		keno(double);			// non-default constructor 
		void playerChoice();	// have player enter 10 choices
		int checkChoice();		// compare player choices to game numbers
		void playKeno();		// game play function 
		void payout(int);		// make appropriate payout
		
	private: 
		int userNumbers[10];	// user entered array of 10 numbers
		int gameNumbers[20];	// game array of 20 randomn numbers
		double startMoney;		// money user enters game with 
		double currentMoney;	// will be updated from base class 
		double currentBet;		// currentBet
		int playAgain();		// checks if user wants to play again
		void kenoArray();		// initializes 20 numbered array
};
#endif
