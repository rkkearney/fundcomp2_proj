/*	Author: 		Reilly Kearney									*
 *	Date:			03/23/2015										*
 *	Description:	Hold 'em Interface File for final project		*/

#ifndef holdem_h
#define holdem_h

#include <iostream>
#include <string>
#include <deque>
#include <iomanip>
using namespace std;

class holdem {
	
	public:
		holdem(); 					// default constructor for holdem class
		holdem( double, double = 500 );
		void initializeDeck();
		void shuffle();
		int getSize();
		void playHoldEm();			// Hold 'Em Interface
	private:
		void deal();				// deals cards
		void flop();				
		void turn();				
		void river();
		int betting();
		int betPrompt( int );		// returns 1 if fold, 0 otherwise
		void ante();
		void fold( int );
		void check( int );
		void raise( int, double = 0 );
		void call( int );
		int playAgain();			// asks user if still playing
		int checkWinner();
		void calculateHandScore( int );
		void reset();
		void newRound( int );
		void printHand( int );		// prints card
		void printTable( int );		// print table
		void printMoney();
		int opponentPlaceBet();		// opponent places bet (returns 1 for fold & 0 otherwise)
		int determineAction();		// opponent determines action (returns int 0 - 3)
		void determineBet();		// oppenent determines value for bet
		void EndOfGame();
		double netChange();			// returns net money lost or gained

		/* Cards */
		struct Card {
			int suitRank;
			int valueRank;
			char suit;
			char value;
		};
		deque< Card > deck;
		
		/* Table */
		Card *table;
		Card *winningHand;
		double pot;					// moentary value in pot
		int canBet;
		
		/* Player */
		struct Player {
			Card *hand;				// player hand
			int handScore;
			double totalBet;		// total bet for game
			double currentBet;		// bet for that round
			double totalMoney;		// opponent's money
		};
		
		double startMoney;			// different from initialMoney

		/* Information from base class - will adjust in future to link*/
		double currentMoney;
};

#endif 