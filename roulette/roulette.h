/*	Author: 		Taylor Rongaus		*
 *	Date:			03/23/2015			*
 *	Description:	Roulette Interface File for final project		*/

#ifndef roulette_h
#define roulette_h
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
using namespace std;

class Roulette {
	public:
		Roulette(); 		// default constructor for roulette class
		void playRoulette();				// function to play the game
		int randomNum();				// return random number the ball lands on
	private:
};

#endif 
