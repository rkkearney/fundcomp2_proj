/*	Author: 		Taylor Rongaus		*
 *	Date:			03/23/2015			*
 *	Description:	Roulette Implementation File for final project	*/

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include "roulette.h"
using namespace std;

// default constructor
Roulette::Roulette() {
}

// return random number the ball lands on
int Roulette::randomNum() {
	srand((int)time(0));
	int r = (rand() % 38);
	return r;
}

// function to play the game
void Roulette::playRoulette() {
	
	// initialize "global" variables
	int payouts[11] = {0, 35, 2, 2, 2, 1, 1, 1, 1, 1, 1};	// matching payouts for 1-10
	int playAgain = 1;			// determine if the user would like to play again or not
	double money = 0.0;		// initialize money won or lost after playing the game
	int RN;							// initialize variable for random number the ball lands on
	
	while (playAgain == 1) {
		
		// initialize local variables-- need to be reset each time through while loop
		int done = 0;				// 0 or 1 to determine if user is done placing chips/ bets
		int i = 0;					// loop counters
		int another = 0;			// 0 or 1 to determine if user wants to place another bet
		int choice[10] = {0};	// array of up to 10 choices for the user's bet
		int number[10];			// array of up to 10 choices for the individual number bet
		int win[10] = {0};		// to be updated w/ corresponding payouts for winning bets
		double userBet[10] = {0};	// initialize bet amount
	
		// run while loop while user continues to place bets
		while (!done) {
			
			// determine the actual value the user wants to bet
			cout << "How much would you like to bet on your chip? ";
			cin >> userBet[i];
			
			// display betting options
			cout << endl;
			cout << "Your options are:" << endl;
			cout << "     1) an individual number: 00, 0, 1, 2 ... 36" << endl;
			cout << "     2) the first 12 numbers" << endl;
			cout << "     3) the second 12 numbers" << endl;
			cout << "     4) the third 12 numbers" << endl;
			cout << "     5) numbers 1 to 18" << endl;
			cout << "     6) numbers 19 to 36" << endl;
			cout << "     7) even numbers" << endl;
			cout << "     8) odd numbers" << endl;
			cout << "     9) red numbers" << endl;
			cout << "     10) black numbers" << endl << endl;
			
			// determine which number above the user chose
			while (choice[i] < 1 || choice[i] > 10) {
				cout << "What option would you like to place this bet on? ";
				cin >> choice[i];
				if (choice[i] < 1 || choice[i] > 10) {
					cout << "Error: please enter a valid choice 1-10. " << endl;
				}
			}
			
			// if choice 1 is chosen, determine which number the user would like to bet on
			if (choice[i] == 1) {
				cout << "Which number 00 - 36 would you like to bet on? " << endl;
				cin >> number[i];
				if (number[i] == 00) {
					number[i] = 37;
				}
			}
			
			// determine if the user wants to place another bet on this round
			cout << "Would you like to place another bet? Enter 1 if yes and 0 if no. " << endl;
			cin >> another;
			
			// checks to see if done placing bets
			if (another != 1) {
				done = 1;
			}
			if (i == 9) {
				cout << "Sorry, you can't place more than 10 bets on this round." << endl;
				done = 1;
			}
			i++;
		}

		// determine the result of the spin using random number generation 0-37 (37=00)
		RN = randomNum();
		if (RN == 37) {
			RN = 00;
		}
		cout << "The number spun is " << RN << endl;

		// check the various user bets to determine if their bet matches the random number condition and update the wins array to the corresponding payout
		for (int j=0; j<10; j++) {
		
			// RN matches number exactly
			if (choice[j] == 1) {
				if (number[j] == RN) {
					win[j] = payouts[1];
				}
				else {
					win[j] = -1 * payouts[1];
				}
			}
			
			// RN matches #s 1-12
			else if (choice[j] == 2) {
				if ((RN >= 1) && (RN <= 12)) {
					win[j] = payouts[2];
				}
				else {
					win[j] = -1 * payouts[2];
				}
			}
			
			// RN matches #s 13-24
			else if  (choice[j] == 3) {
				if ((RN >= 13) && (RN <= 24)) {
					win[j] = payouts[3];
				}
				else {
					win[j] = -1 * payouts[3];
				}
			}
			
			// RN matches #s 25-36
			else if (choice[j] == 4) {
				if ((RN >= 25) && (RN <= 36)) {
					win[j] = payouts[4];
				}
				else {
					win[j] = -1 * payouts[4];
				}
			}
			
			// RN matches #s 1-18
			else if (choice[j] == 5) {
				if ((RN >= 1) && (RN <= 18)) {
					win[j] = payouts[5];
				}
				else {
					win[j] = -1 * payouts[5];
				}
			}
			
			// RN matches #s 19-36
			else if (choice[j] == 6) {
				if ((RN >= 19) && (RN <= 36)) {
					win[j] = payouts[6];
				}
				else {
					win[j] = -1 * payouts[6];
				}
			}
			
			// RN matches even #s
			else if (choice[j] == 7) {
				if (RN % 2 == 0) {
					win[j] = payouts[7];
				} 
				else {
					win[j] = -1 * payouts[7];
				}
			}
			
			// RN matches odd #s
			else if (choice[j] == 8) {
				if (RN % 2 != 0) {
					win[j] = payouts[8];
				} 
				else {
					win[j] = -1 * payouts[8];
				}
			}
			
			// RN matches a red #
			else if (choice[j] == 9) {
				if ((RN == 1) || (RN == 3) || (RN == 5) || (RN == 7) || (RN == 9) || (RN == 12) || (RN == 14) || (RN == 16) || (RN == 18) || (RN == 19) || (RN == 21) || (RN == 23) || (RN == 25) || (RN == 27) || (RN == 30) || (RN == 32) || (RN == 34) || (RN == 36)) {
					win[j] = payouts[9];
				}
				else {
					win[j] = -1 * payouts[9];
				}
			}
			
			// RN matches a black #
			else if (choice[j] == 10) {
				if ((RN == 2) || (RN == 4) || (RN == 6) || (RN == 8) || (RN == 10) || (RN == 11) || (RN == 13) || (RN == 15) || (RN == 17) || (RN == 20) || (RN == 22) || (RN == 24) || (RN == 26) || (RN == 28) || (RN == 29) || (RN == 31) || (RN == 33) || (RN == 35)) {
					win[j] = payouts[10];
				}
				else {
					win[j] = -1 * payouts[10];
				}
			}
		}
	
		// Determine if the user's bets won or lost & indicate amount won or lost
		for (int k=0; k<10; k++) {
			if (win[k] > 0) {
				cout << "Congrats! One of your bets won! You will receive $" << win[k]*userBet[k] << endl;
				money = money + win[k]*userBet[k];
			}
			else if (win[k] < 0) {
				cout << "So sorry. One of your bets lost. You will be deducted $" << userBet[k] << endl;
				money = money - userBet[k];
			}
		}
		
		cout << endl;
		cout << "Your current balance is $" << money << endl;
		cout << "Play again? Enter 1 if yes or 0 if no. " << endl;
		cin >> playAgain;
	}
}
