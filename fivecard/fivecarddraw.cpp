#include <iostream>
#include <iomanip>
#include <string>
#include <deque>
#include <algorithm>
#include <stdlib.h>
#include <unistd.h>
#include "fivecarddraw.h"
using namespace std;

FiveCard::FiveCard() {

}

FiveCard::FiveCard( double curr ) {
	pot = 0;
	
	/* Initialize Player */
	player.totalBet   = 0;
	player.currentBet = 0;
	player.totalMoney = curr;
	startMoney        = curr;
	currentMoney      = curr;

	/* Initialize Opponent */
	opponent.totalBet   = 0;
	opponent.currentBet = 0;
	opponent.totalMoney = 1000;

	initializeDeck();
}

void FiveCard::initializeDeck() {
	int rank = 0;
	Card card;
	for ( int j = 0; j < 13; j++ ) {
		for ( int i = 0; i < 4; i++ ) {
			card.suitRank  = i;
			card.number = j;
			card.valueRank = rank++;
			
			/* Suits */
			if      ( i == 0 ) { card.suit = 'C'; }
			else if ( i == 1 ) { card.suit = 'D'; }
			else if ( i == 2 ) { card.suit = 'H'; }
			else if ( i == 3 ) { card.suit = 'S'; }
			
			/* Value */
			if      ( j  <  8 ) { card.value = '0' + j + 2; }
			else if ( j ==  8 ) { card.value = 'T'; }
			else if ( j ==  9 ) { card.value = 'J'; }
			else if ( j == 10 ) { card.value = 'Q'; }
			else if ( j == 11 ) { card.value = 'K'; }
			else if ( j == 12 ) { card.value = 'A'; }

			/* Add card to deck */
			deck.push_back( card );
		}
	}
	shuffle();
}

void FiveCard::shuffle() {
	random_shuffle( deck.begin(), deck.end() );
}

void FiveCard::playGame() {
	int winner;
	//title();

	while ( 1 ) {
		ante();
		deal();
		printHand( 0 );
		//betting
		draw();
		//opponent draws
		printHand( 0 );
		//betting
		printHand( 1 );
		winner = checkWinner();
		printHand( winner + 2 );

		if ( !playAgain() ) {
			//END OF GAME
			//SEND BACK TO CASINO
			return;
		}
		reset();
	}
}

void FiveCard::deal() {
	cout << "Dealing...\n\n";
	sleep( 2 );
	player.hand   = new Card[ 5 ];
	opponent.hand = new Card[ 5 ];

	for ( int i = 0; i < 5; i++ ) {
		player.hand[ i ] = deck.front();
		deck.pop_front();					// delete card dealt
	}
	for ( int i = 0; i < 5; i++ ) {
		opponent.hand[ i ] = deck.front();
		deck.pop_front();					// delete card dealt
	}
}

void FiveCard::draw() {
	int choice, discard;
	cout << "Would you like to:\n(1) Stand Pat\t(2) Draw Cards\n";
	cin >> choice;

	if ( choice == 2 ) {
		cout << "How many cards would you like to draw: ";
		cin >> choice;
		if ( choice < 5 ) {
			for ( int i = 0; i < choice; i++ ) {
				cout << "Choose Card to Discard ( 1 - 5 ): ";
				cin >> discard;
				player.hand[ discard - 1 ] = deck.front();
				deck.pop_front();
			}
		}
		else {
			for ( int i = 0; i < 5; i++ ) {
				player.hand[ i ] = deck.front();
				deck.pop_front();
			}
		}
		cout << "Drawing Cards...\n";
		sleep( 2 );
	}
}

void FiveCard::ante() {
	cout << "Ante Up!" << endl;
	player.totalMoney   -= 30;
	player.totalBet     += 30;
	opponent.totalMoney -= 30;
	opponent.totalBet   += 30;
	pot                 += 60;
	sleep( 1 );
	cout << "Pot: " << pot << endl;
}

void FiveCard::betPrompt() {
	int choice;
	
	cout << "Would you like to:\n(1) Fold\t(2) Check\t(3) Bet/Call\n";
	cin >> choice;

	cout << "Would you like to:\n(1) Fold\t(3) Bet/Call\n";
	cin >> choice;
	
	/*switch ( choice ) {
		case 1:
			fold();
			break;
		case 2:
			break;
		case 3:
			betorcall();
			break;
	}*/
}

int FiveCard::playAgain() {
	char c;
	cout << "\nYour Current Money: " << player.totalMoney << endl << endl;
	sleep( 1 );
	cout << "Would you like to play again? (Y/N) ";
	cin >> c;

	if      ( c == 'Y' || c == 'y' ) { return 1; }
	else if ( c == 'N' || c == 'n' ) { return 0; }
	else {
		cout << "Invalid input." << endl;
		return playAgain();
	}
}

int FiveCard::checkWinner() {
	cout << "\nDetermining winner...\n";
	sleep ( 2 );
	
	calculateHandScore();
	
	if ( player.handScore > opponent.handScore ) {			// player wins
		player.totalMoney += pot;
		return 0;
	}	
	else if ( player.handScore < opponent.handScore ) {		// opponent wins
		opponent.totalMoney += pot;
		return 1;
	}	
	else {
		player.totalMoney   += pot/2;
		opponent.totalMoney += pot/2;
		return 2;	// for compiling
	}
	
}

void FiveCard::calculateHandScore() {
	/* Player */
	if      ( checkStraight( 0 ) && checkFlush( 0 ) ) { player.handType =  "Straight Flush"; }
	else if (                 checkFourofaKind( 0 ) ) { player.handType =  "Four of a Kind"; }
	else if (                   checkFullHouse( 0 ) ) { player.handType =      "Full House"; }
	else if (                       checkFlush( 0 ) ) { player.handType =           "Flush"; }
	else if (                    checkStraight( 0 ) ) { player.handType =        "Straight"; }
	else if (                checkThreeofaKind( 0 ) ) { player.handType = "Three of a Kind"; }
	else if (                     checkTwoPair( 0 ) ) { player.handType =        "Two Pair"; }
	else if (                     checkOnePair( 0 ) ) { player.handType =        "One Pair"; }
	else if (                    checkHighCard( 0 ) ) { player.handType =       "High Card"; }
	
	/* Opponent */
	if      ( checkStraight( 1 ) && checkFlush( 1 ) ) { opponent.handType =  "Straight Flush"; }
	else if (                 checkFourofaKind( 1 ) ) { opponent.handType =  "Four of a Kind"; }
	else if (                   checkFullHouse( 1 ) ) { opponent.handType =      "Full House"; }
	else if (                       checkFlush( 1 ) ) { opponent.handType =           "Flush"; }
	else if (                    checkStraight( 1 ) ) { opponent.handType =        "Straight"; }
	else if (                checkThreeofaKind( 1 ) ) { opponent.handType = "Three of a Kind"; }
	else if (                     checkTwoPair( 1 ) ) { opponent.handType =        "Two Pair"; }
	else if (                     checkOnePair( 1 ) ) { opponent.handType =        "One Pair"; }
	else if (                    checkHighCard( 1 ) ) { opponent.handType =       "High Card"; }
}

int FiveCard::checkFourofaKind( int x ) {
	int count = 0, score = 0;
	char match;
	if ( !x ) {
		for ( int j = 0; j < 2; j++ ) {
			match = player.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 4 ) {
				player.handScore = score;
				return 1;
			}
			count = 0;
		}
	}
	else if ( x ) {
		for ( int j = 0; j < 2; j++ ) {
			match = opponent.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 4 ) {
				opponent.handScore = score;
				return 1;
			}
			count = 0;
		}
	}
	return 0;
}

int FiveCard::checkFullHouse( int x ) {
	/* Check Three of a Kind */
	int count = 0, score = 0;
	char match, previous;
	int check = 0;
	if ( !x ) {
		for ( int j = 0; j < 3; j++ ) {
			match = player.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 3 ) {
				player.handScore = score;
				check = 1;
				previous = match;
				break;
			}
			count = 0;
		}
	}
	else if ( x ) {
		for ( int j = 0; j < 3; j++ ) {
			match = opponent.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 3 ) {
				opponent.handScore = score;
				check = 1;
				previous = match;
				break;
			}
			count = 0;
		}
	}

	/* Check Pair */
	count = 0;
	score = 0;
	if ( !x && check ) {
		check = 0;
		for ( int j = 0; j < 4; j++ ) {
			match = player.hand[ j ].value;
			if ( match == previous ) { continue; }
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				player.handScore += score;
				check = 1;
			}
			count = 0;
		}
	}
	else if ( x && check ) {
		check = 0;
		for ( int j = 0; j < 4; j++ ) {
			match = opponent.hand[ j ].value;
			if ( match == previous ) { continue; }
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				opponent.handScore += score;
				check = 1;
			}
			count = 0;
		}
	}
	return check;
}

int FiveCard::checkFlush( int x ) {
	if ( !x ) {
		for ( int i = 1; i < 5; i++ ) {
			if ( player.hand[ 0 ].suitRank != player.hand[ i ].suitRank ) { return 0; }
		}
		for ( int i = 0; i < 5; i++ ) {
			player.handScore += player.hand[ i ].valueRank;
		}
	}
	else if ( x ) {
		for ( int i = 1; i < 5; i++ ) {
			if ( opponent.hand[ 0 ].suitRank != opponent.hand[ i ].suitRank ) { return 0; }
		}
		for ( int i = 0; i < 5; i++ ) {
			opponent.handScore += opponent.hand[ i ].valueRank;
		}
	}
	return 1;
}

int FiveCard::checkStraight( int x ) {
	int values[ 5 ];
	int check = 1;
	if ( x ) {
		for ( int i = 0; i < 5; i++ ) {
			values[ i ] = player.hand[ i ].number;
		}
	}
	else if ( !x ) {
		for ( int i = 0; i < 5; i++ ) {
			values[ i ] = opponent.hand[ i ].number;
		}
	}

	sort( values, values + 5 );
	
	for ( int i = 0; i < 4; i++ ) {
		if ( values[ i ] != ( values[ i + 1 ] - 1 ) ) { check = 0; }
	}
	
	if ( !check && ( find( values, values + 5, 12 ) != ( values + 5 ) ) ) {
		replace( values, values + 5, 12, -1 );
		sort( values, values + 5 );
		check = 1;
		for ( int i = 0; i < 4; i++ ) {
			if ( values[ i ] != ( values[ i + 1 ] - 1 ) ) { check = 0; }
		}
	}
	
	return check;
}

int FiveCard::checkThreeofaKind( int x ) {
	int count = 0, score = 0;
	char match;
	if ( !x ) {
		for ( int j = 0; j < 3; j++ ) {
			match = player.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 3 ) {
				player.handScore = score;
				return 1;
			}
			count = 0;
		}
	}
	else if ( x ) {
		for ( int j = 0; j < 3; j++ ) {
			match = opponent.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 3 ) {
				opponent.handScore = score;
				return 1;
			}
			count = 0;
		}
	}
	return 0;
}

int FiveCard::checkTwoPair( int x ) {
	int count = 0, score = 0, pairs = 0;
	char match, previous = 'g';
	if ( !x ) {
		for ( int j = 0; j < 4; j++ ) {
			match = player.hand[ j ].value;
			if ( match == previous ) { continue; }
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				pairs++;
				if ( pairs == 2 ) {
					player.handScore = ( score ) * ( .9 );
					return 1;
				}
				previous = match;
			}
			count = 0;
		}
	}
	else if ( x ) {
		for ( int j = 0; j < 4; j++ ) {
			match = opponent.hand[ j ].value;
			if ( match == previous ) { continue; }
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				pairs++;
				if ( pairs == 2 ) {
					opponent.handScore = ( score ) * ( .9 );
					return 1;
				}
				previous = match;
			}
			count = 0;
		}
	}
	return 0;
}

int FiveCard::checkOnePair( int x ) {
	int count = 0, score = 0;
	char match;
	if ( !x ) {
		for ( int j = 0; j < 4; j++ ) {
			match = player.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				player.handScore = score;
				return 1;
			}
			count = 0;
		}
	}
	else if ( x ) {
		for ( int j = 0; j < 4; j++ ) {
			match = opponent.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				opponent.handScore = score;
				return 1;
			}
			count = 0;
		}
	}
	return 0;
}

int FiveCard::checkHighCard( int x ) {
	int high = 0;
	if ( !x ) {
		for ( int i = 0; i < 5; i++ ) {
			if ( player.hand[ i ].valueRank > high ) { high = player.hand[ i ].valueRank; }
		}
		player.handScore = high;
	}
	else if ( x ) {
		for ( int i = 0; i < 5; i++ ) {
			if ( opponent.hand[ i ].valueRank > high ) { high = opponent.hand[ i ].valueRank; }
		}
		opponent.handScore = high;
	}
	return 1;
}

void FiveCard::reset() {
	cout << "\nReshuffling...\n";
	sleep ( 2 );
	initializeDeck();
	pot = 0;
	delete[] player.hand;
}

void FiveCard::printHand( int n ) {
	if ( n == 2 ) {
		cout << "\nYou win!!\t" << player.handType << "\n";
		n -= 2;
	}
	else if ( n == 3 ) {
		cout << "\nOpponent Wins.\t" << opponent.handType << "\n";
		n -= 2;
	}
	else if ( n == 0 ) { cout << "\nYour Hand:\n"; }
	else if ( n == 1 ) { cout << "\nOpponent Hand:\n"; }
	
	switch ( n ) {
		cout << endl;
		case 0:		// print just player hand
			cout << "  ---   ---   ---   ---   --- \n";
			cout << " | " << player.hand[ 0 ].value  << " | | " << player.hand[ 1 ].value << " | | " << player.hand[ 2 ].value << " | | " << player.hand[ 3 ].value << " | | " << player.hand[ 4 ].value << " | " << endl;
			cout << " | " << player.hand[ 0 ].suit   << " | | " << player.hand[ 1 ].suit  << " | | " << player.hand[ 2 ].suit  << " | | " << player.hand[ 3 ].suit  << " | | " << player.hand[ 4 ].suit  << " | " << endl;
			cout << "  ---   ---   ---   ---   --- \n";
			cout << "  (1)   (2)   (3)   (4)   (5) \n";
			break;
		case 1:		// print opponent hand
			cout << "  ---   ---   ---   ---   --- \n";
			cout << " | " << opponent.hand[ 0 ].value  << " | | " << opponent.hand[ 1 ].value << " | | " << opponent.hand[ 2 ].value << " | | " << opponent.hand[ 3 ].value << " | | " << opponent.hand[ 4 ].value << " | " << endl;
			cout << " | " << opponent.hand[ 0 ].suit   << " | | " << opponent.hand[ 1 ].suit  << " | | " << opponent.hand[ 2 ].suit  << " | | " << opponent.hand[ 3 ].suit  << " | | " << opponent.hand[ 4 ].suit  << " | " << endl;
			cout << "  ---   ---   ---   ---   --- \n";
			break;
	}
}