#ifndef holdem_h
#define holdem_h

#include <iostream>
#include <string>
#include <deque>
#include <iomanip>
using namespace std;

class FiveCard {
	public:
		FiveCard(); 					// default constructor for holdem class
		FiveCard( double );
		void initializeDeck();
		void shuffle();
		//int getSize();
		void playGame();			// gameplay interface
	private:
	//	void title();
		void deal();				// deals cards
		void draw();
		void ante();
		void betPrompt();
		int playAgain();			// asks user if still playing
		int checkWinner();
		void calculateHandScore();
		int checkFourofaKind( int );
		int checkFullHouse( int );
		int checkFlush( int );
		int checkStraight( int );
		int checkThreeofaKind( int );
		int checkTwoPair( int );
		int checkOnePair( int );
		int checkHighCard( int );
		void reset();
		void printHand( int );		// prints card
		//void printMoney();
		//void EndOfGame();
		//double netChange();			// returns net money lost or gained

		/* Cards */
		struct Card {
			int suitRank;
			int valueRank;
			int number;
			char suit;
			char value;
		};
		deque< Card > deck;
		
		double pot;

		/* Player */
		struct Player {
			Card *hand;				// player hand
			int handScore;
			string handType;		// for printing
			double totalBet;		// total bet for game
			double currentBet;		// bet for that round
			double totalMoney;		// total money
		};

		Player player;
		Player opponent;
		
		double startMoney;			// different from initialMoney

		/* Information from base class - will adjust in future to link*/
		double currentMoney;
};


#endif 
