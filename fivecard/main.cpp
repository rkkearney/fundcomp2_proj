#include <iostream>
#include <iomanip>
#include <string>
#include <deque>
#include <algorithm>
#include <stdlib.h>
#include "fivecarddraw.h"

using namespace std;

int main() {
	FiveCard game( 500 );

	game.playGame();
	return 0;
}