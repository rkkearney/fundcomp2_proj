// fivecardstud.h
// Author: Reilly Kearney
// class for composition in casino

#ifndef fivecardstud_h
#define fivecardstud_h

#include <iostream>
#include <string>
#include <deque>
#include <iomanip>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include "casino.h"
using namespace std;

class FiveCard {
	public:
		FiveCard();
		void initializeGame( double );	// set variables for gameplay
		void initializeDeck();			// initalize deque of structs for gameplay
		void shuffle();					// shuffle cards
		double playFiveCard( double );	// gameplay Interface
		void initPrint();				// print for start of game
	private:
		void deal();					// deals cards
		void draw();					// prompts and draws cards
		void opponentDraw();			// draw cards for opponent
		void ante();					// initial bets
		int betPrompt( int );			// prompt for bet
		int opponentBet( int );			// opponent determines bet
		int playAgain();				// asks user if still playing
		int checkWinner( int, int );	// checks for winner by comparing handScores
		void calculateHandScore();		// calculates the handScores for both players
		int checkFourofaKind( int );	// checks hands for 4 of a kind
		int checkFullHouse( int );		// checks hands for Full House
		int checkFlush( int );			// checks hands for flush
		int checkStraight( int );		// checks hands for straight
		int checkThreeofaKind( int );	// checks hands for 3 of a kind
		int checkTwoPair( int );		// checks hands for 2 pair
		int checkOnePair( int );		// checks hands for pair
		int checkHighCard( int );		// checks hands for high card
		void reset();					// resets for next round
		void printHand( int );			// prints card

		/* Cards */
		struct Card {
			int suitRank;				// rank of suit
			int valueRank;				// rank of card in deck
			int number;					// number (for straight)
			char suit;					// suit for print
			char value;					// value for print
		};
		deque< Card > deck;				// deque of structs
		
		double pot;						// pot to hold bets

		/* Player */
		struct Player {
			Card *hand;					// player hand
			int handScore;				// hand score (strength of hand)
			string handType;			// for printing
			double currentBet;			// bet for that round
			double totalMoney;			// total money
		};
		Player player;					// player
		Player opponent;				// opponent
};

#endif
