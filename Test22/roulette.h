// roulette.h (class definition)
// Due 2/8/16
// Author: Taylor Rongaus
// Derived class of casino

#ifndef roulette_H
#define roulette_H
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <unistd.h>
#include <string>
#include <cstring>
#include "casino.h"
using namespace std;

// Derived roulette class; inherits from casino
class roulette {
	public:
		roulette(); 				// default constructor for roulette class
		double playRoulette(double);		// function to play the game
		int randomNum();						// return random number the ball lands on
		void initPrint();
		int again();
	private:
};

#endif
