// blackjack.cpp
// Due 2/8/16
// Author: Reilly Kearney and Taylor Rongaus
// class for composition in casino

#include <stdlib.h>
#include "casino.h"
#include "blackjack.h"

using namespace std;

// default constructor for the blackjack class
blackjack::blackjack() {

}

void blackjack::initializeGame( double curr ) {
	/* Initialize Player Values */
	player.currentBet = 0;
	player.totalMoney = curr;
	for ( int i = 0; i < 2; i++ ) { player.handScore[ i ] = 0; }
	player.size       = 0;

	/* Initialize Dealer Values */
	dealer.currentBet = 0;
	dealer.totalMoney = 0;
	for ( int i = 0; i < 2; i++ ) { dealer.handScore[ i ] = 0; }
	dealer.size       = 0;

	initializeDeck();				// itialize deck (52 cards)
}

void blackjack::initPrint() {
	// set the name of the file to print
	string file = "blackjack_word.txt";
	// print the file to stdout
	string tmp;
	ifstream myFile ( file.c_str() );
	cout << endl;
	if ( myFile.is_open() ) {
		while( myFile.good() ) {
			getline( myFile, tmp );
			cout << tmp << endl;
		}
	}
}

void blackjack::initializeDeck() {
	Card card;								// temporary Card struct for initialization
	for ( int j = 0; j < 13; j++ ) {		// go through values
		for ( int i = 0; i < 4; i++ ) {		// go through suits
			card.suitRank  = i;								// set value for suit of card
			if ( j == 0 ) { card.valueRank = 11; }			// set value to 11 if Ace
			else if ( j > 8 ) { card.valueRank = 10; }		// set value to 10 if King, Queen, King 
			else { card.valueRank = j + 1; }				// set value to numerical value
			
			/* Suits for Printing */
			if      ( i == 0 ) { card.suit = 'C'; }		// clubs
			else if ( i == 1 ) { card.suit = 'D'; }		// diamonds
			else if ( i == 2 ) { card.suit = 'H'; }		// hearts
			else if ( i == 3 ) { card.suit = 'S'; }		// spades
			
			/* Value for Printing */
			if      ( j ==  0 ) { card.value = 'A'; }			// ace
			else if	( j <=  8 ) { card.value = '0' + j + 1; }	// number
			else if ( j ==  9 ) { card.value = 'T'; }			// ten
			else if ( j == 10 ) { card.value = 'J'; }			// jack
			else if ( j == 11 ) { card.value = 'Q'; }			// queen
			else if ( j == 12 ) { card.value = 'K'; }			// king

			/* Add card to deck */
			deck.push_back( card );
		}
	}
	shuffle();
}

void blackjack::shuffle() {
	random_shuffle( deck.begin(), deck.end() );		// shuffles deck
}

int blackjack::getSize() {
	return deck.size();
}

double blackjack::playBlackJack( double curr ) {
	int dealerTurn = 1;							// 1 for keep asking player, 0 for move onto dealer	
	int check, winner;
	initializeGame( curr );		// intialize variables for dealer and player

	while ( 1 ) {
		/* Print Money */
		cout << "\nYou currently have $" << player.totalMoney << endl;

		/* Place Bet */
		cout << "Place your bet: $";
		cin >> player.currentBet;
		if ( !placeBet( player.currentBet ) ) { continue; }		// prompt valid bet

		/* Deal Initial Cards */
		Deal();
		printHand( 0 );		// print player hand
		
		/* Check for Player BlackJack */
		if ( check21( 'P' ) ) {
			dealerTurn = 0;
		}
		/* Player Turn */
		else {
			sleep( 1 );
			printHand( -1 );	// print dealer hand, face down
			while ( 1 ) { 
				if ( player.handScore[ 0 ] == 11 ) {
					if ( checkDoubleDown() ) {	// prompt user for double down
						doubleDown();			// double down
						sleep( 1 );				
						printHand( 0 );			// print new user hand
						break;
					}
				}
				check = checkHit();				// prompt users to hit
				if ( check ) {					// if user wants card,
					hit( 'P' );					// deal card to user
					sleep ( 1 );
					printHand( 0 );				// print user new hand
				}
				if ( player.handScore[ 1 ] > 21 ) { break; }	// check if player bust
				if ( !check ) { break; }
			}
		}

		/* Dealer Turn */
		if ( dealerTurn	) {
			printHand( 1 );		// print dealer hand, both cards
			/* Check for Dealer BlackJack */
			if ( !check21( 'D' ) && dealer.handScore[ 1 ] < 17 ) {
				while ( dealer.handScore[ 1 ] < 17 ) {			// if dealer under 17
					hit( 'D' );									// hit, add card to dealer hand
					cout << "Dealer hits...\n";
					sleep ( 1 );
				}
				cout << "Dealer stands.\n";
				printHand( 1 );		// print new dealer hand
			}
			else if ( dealer.handScore[ 1 ] >= 17 ) { cout << "Dealer stands.\n"; }
		}

		/* Check for Winner */
		checkWinner();
	
		/* Print Money */
		cout << "You now have: $" << player.totalMoney << endl;

		/* Prompt for Continue Play */
		if (player.totalMoney <= 0) {break;};
		if ( !playAgain() ) {
			/* Return money to Casino */
			return player.totalMoney;
		}
		else {
			/* Check Deck Size */
			if ( getSize() <= 15 ) { initializeDeck(); }	// reinitialize and shuffle
			/* Reset GamePlay */
			reset();
			dealerTurn = 1;
		}
	}
}

int blackjack::placeBet( double bet ) {
	if (bet > player.totalMoney) { return 0; }		// if bet greater than money
	else {
		player.currentBet = bet;
		player.totalMoney -= player.currentBet;		// subtrace bet from money
		return 1;
	}
}

void blackjack::Payout( int n ) {
	switch (n) {
		case 0:
			break;
		case 1: // user won
			player.totalMoney = player.totalMoney + (2 * player.currentBet);	// paid out
			break;
		case 2: // pushed
			player.totalMoney = player.totalMoney + player.currentBet;			// break even
			break;
		case 3: // blackjack
			player.totalMoney = player.totalMoney + (3 * player.currentBet);	// paid out double
	}
}

void blackjack::Deal() {
	cout << "Dealing...\n";
	sleep( 1 );

	player.hand = new Card[ 10 ];		// create array for cards for player
	dealer.hand = new Card[ 10 ];		// create array for cards for dealer

	for ( int i = 0; i < 2; ++i ) {
		player.hand[ i ] = deck.front();	// deal card
		if ( deck.front().valueRank == 11 ) { player.handScore[ 1 ] += 1; }		// for ace value
		else { player.handScore[ 1 ] += player.hand[ player.size ].valueRank; }
		player.handScore[ 0 ] += player.hand[ player.size++ ].valueRank;		// add to handscore
		deck.pop_front();					// delete card dealt
	}
	for ( int i = 0; i < 2; i++ ) {
		dealer.hand[ i ] = deck.front();	// deal card
		if ( deck.front().valueRank == 11 ) { dealer.handScore[ 1 ] += 1; }		// for ace value
		else { dealer.handScore[ 1 ] += dealer.hand[ dealer.size ].valueRank; }
		dealer.handScore[ 0 ] += dealer.hand[ dealer.size++ ].valueRank;		// add to handscore
		deck.pop_front();					// delete card dealt
	}
}

int blackjack::playAgain() {
	char c;
	cout << "Would you like to play again? (Y/N) ";
	cin >> c;
	if (c == 'Y' || c == 'y') { return 1; }
	else if (c == 'N' || c == 'n') { return 0; }
	return 0;
}

int blackjack::checkWinner () {
	cout << endl;
	int playerHand, dealerHand;

	/* Logic for Ace as 1 or 11 */
	if ( ( player.handScore[ 0 ] <= 21 ) && ( player.handScore[ 1 ] <= 21 ) ) {
		if ( player.handScore[ 0 ] > player.handScore[ 1 ] ) { playerHand = player.handScore[ 0 ]; }
		else { playerHand = player.handScore[ 1 ]; }
	}
	else if ( ( player.handScore[ 0 ] > 21 ) && ( player.handScore[ 1 ] <= 21 ) ) { playerHand = player.handScore[ 1 ]; }
	else { playerHand = player.handScore[ 0 ]; }
	if ( ( dealer.handScore[ 0 ] <= 21 ) && ( dealer.handScore[ 1 ] <= 21 ) ) {
		if ( dealer.handScore[ 0 ] > dealer.handScore[ 1 ] ) { dealerHand = dealer.handScore[ 0 ]; }
		else { dealerHand = dealer.handScore[ 1 ]; }
	}
	else if ( ( dealer.handScore[ 0 ] > 21 ) && ( dealer.handScore[ 1 ] <= 21 ) ) { dealerHand = dealer.handScore[ 1 ]; }
	else { dealerHand = dealer.handScore[ 0 ]; }

	if ( playerHand <= 21 ) {
		if ( dealerHand > 21 ) {
			cout << "Congrats! Dealer Bust! You Win!" << endl;
			Payout(1);		// paid 1 to 1
			return 1;
		}
		else if ( playerHand > dealerHand ) {
			cout << "Congrats! You Win!" << endl;
			Payout(1);		// paid 1 to 1
			return 1;
		}
		else if ( playerHand < dealerHand ) {
			cout << "House Wins! Better Luck next time!" << endl;
			Payout(0);		// paid nothing
			return 0;
		}
		else if ( playerHand == dealerHand ) {
			cout << "You Pushed!" << endl;
			Payout(2);		// break even
			return 2;
		}
	}
	else if ( ( playerHand > 21 ) && ( dealerHand > 21 ) ) {
		cout << "You and the House bust!!" << endl;
		Payout(2);		// break even
		return 2;
	}
	else if ( playerHand > 21 ) {
		cout << "You Bust! House Wins! Better Luck next time!" << endl;
		return 0;
	}

	return 0;
}

int blackjack::checkHit() {
	char c;
	cout << "\nWould you like to hit? (Y/N) ";
	cin >> c;
	if ( c == 'Y' || c == 'y' ) { return 1; }
	else if ( c == 'N' || c == 'n' ) { return 0; }
	else { return 0; }
}

int blackjack::checkDoubleDown() {
	char c;
	cout << "\nWould you like to double down? (Y/N) ";
	cin >> c;
	if ( c == 'Y' || c == 'y' ) { return 1; }
	else if ( c == 'N' || c == 'n' ) { return 0; }
	else { return 0; }
}

int blackjack::check21( char z ) {
	if ( player.handScore[ 0 ] == 21 && z == 'P' ) {
		cout << "YOU GOT BLACKJACK!" << endl;
		Payout(3);				// paid 2 to 1
		return 1;
	}
	else if ( dealer.handScore[ 0 ] == 21 && z == 'D' ) {
		cout << "DEALER GOT BLACKJACK!" << endl;
		Payout(0);				// paid nothing
		return 1;
	}
	else { return 0; }
}

void blackjack::hit( char p ) {
	if ( p == 'P' ) {
		player.handScore[ 0 ] += deck.front().valueRank;		// add card to player handscore
		if ( deck.front().valueRank == 11 ) { player.handScore[ 1 ] += 1; }		// for ace value
		else { player.handScore[ 1 ] += deck.front().valueRank; }
		player.hand[ player.size++ ] = deck.front();	// add card to player hand
		deck.pop_front();								// delete card from deque
	}
	else if ( p == 'D' ) {
		dealer.handScore[ 0 ] += deck.front().valueRank;		// add card to dealer handscore
		if ( deck.front().valueRank == 11 ) { dealer.handScore[ 1 ] += 1; }		// for ace value
		else { dealer.handScore[ 1 ] += deck.front().valueRank; }
		dealer.hand[ dealer.size++ ] = deck.front();	// add card to dealer hand
		deck.pop_front();								// delete card from deque
	}
}

void blackjack::doubleDown() {
	player.currentBet += player.currentBet;			// double bet

	player.handScore[ 0 ] += deck.front().valueRank;						// add card to player handscore
	if ( deck.front().valueRank == 11 ) { player.handScore[ 1 ] += 1; }		// for ace value
	else { player.handScore[ 1 ] += deck.front().valueRank; }
	player.hand[ player.size++ ] = deck.front();	// add card to player hand
	deck.pop_front();								// delete card from deque
}

void blackjack::printHand( int n ) {
	switch ( n ) {
		cout << endl;
		case 0:		// print just player hand
			cout << "\nYour Hand:\n";
			for ( int i = 0; i < player.size; i++ ) { cout << "  ---  "; }
			cout << endl;
			for ( int i = 0; i < player.size; i++ ) { cout << " | " << player.hand[ i ].value  << " | "; }
			cout << endl;
			for ( int i = 0; i < player.size; i++ ) { cout << " | " << player.hand[ i ].suit   << " | "; }
			cout << endl;
			for ( int i = 0; i < player.size; i++ ) { cout << "  ---  "; }
			cout << endl;
			break;
		case 1:		// print dealer hand finish
			cout << "\nDealer Hand:\n";
			for ( int i = 0; i < dealer.size; i++ ) { cout << "  ---  "; }
			cout << endl;
			for ( int i = 0; i < dealer.size; i++ ) { cout << " | " << dealer.hand[ i ].value  << " | "; }
			cout << endl;
			for ( int i = 0; i < dealer.size; i++ ) { cout << " | " << dealer.hand[ i ].suit   << " | "; }
			cout << endl;
			for ( int i = 0; i < dealer.size; i++ ) { cout << "  ---  "; }
			cout << endl;
			break;
		case -1:		// print dealer hand start
			cout << "\nDealer Hand:\n";
			cout << "  ---   --- \n";
			cout << " | " << dealer.hand[ 0 ].value  << " | |   | \n";
			cout << " | " << dealer.hand[ 0 ].suit   << " | |   | \n";
			cout << "  ---   --- \n";
			break;
	}
	cout << "\n";
}

void blackjack::reset() {
	delete[] player.hand;		// release player hand for next round
	delete[] dealer.hand;		// release dealer hand for next round
	player.currentBet = 0;		// reset player bet for next round
	for ( int i = 0; i < 2; i++ ) {
		player.handScore[ i ] = 0;		// reset player score for next round
		dealer.handScore[ i ] = 0;		// reset dealer score for next round
	}
	player.size = 0;			// reset player size iterator for next round
	dealer.size = 0;			// reset dealer size iterator for next round
}


