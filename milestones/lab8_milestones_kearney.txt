Author:	Reilly Kearney
Date:	03/02/2016
lab8_milestones_kearney.txt contains the rubric for 40 points with weighting for tasks for my final project
 
Lab 8 Milestones:

	20 points:
		- Read through the Lazy Foo SDL tutorials
		- Complete a problem as a deliverable (non-tutorial)

	10 points:
		- Create an initial outline of all the variables and classes (base class and inherited classes)
				~ Work with members (google doc)

	5 points:
		- Initialize the class for the Player/Casino (base class)

	5 points:
		- Initialize (basic) classes for at least one game that inherits from Player/Casino base class (cover what is not already covered)