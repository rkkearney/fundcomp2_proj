/*	Author: 		Reilly Kearney									*
 *	Date:			03/23/2015										*
 *	Description:	Black Jack Driver File for final project		*/

#include <iostream>
#include <iomanip>
#include <string>
#include <deque>
#include <algorithm>
#include <stdlib.h>
#include "blackjack.h"

using namespace std;

int main() {
	srand(time(NULL));

	blackjack game(500, 52);
	game.playBlackJack();
	

}