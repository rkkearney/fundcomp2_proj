/*	Author: 		Reilly Kearney and Taylor Rongaus		*
 *	Date:			03/23/2015										*
 *	Description:	Roulette Interface File for final project		*/

#ifndef roulette_h
#define roulette_h

#include <iostream>
#include <iomanip>
using namespace std;

class roulette : public casino{
	
	public:
		roulette(); 					// default constructor for roulette class
		double getBet();		// function to get each of the user's bets
		void print();						// test print function
	private:
		
};

#endif 
