// blackjack.h (class definition)
// Due 2/8/16
// Author: Reilly Kearney and Taylor Rongaus
// class for composition in casino

#ifndef blackjack_H
#define blackjack_H
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <deque>
#include <algorithm>
#include <stdlib.h>
#include <unistd.h>
#include "casino.h"
using namespace std;

// Derived blackjack class; inherits from casino
class blackjack {
	public:
		blackjack(); 						// default constructor for blackjack class
		void initializeGame( double );		// sets values for start of game
		void initializeDeck();				// creates the deque of 52 Cards for gameplay
		void initPrint();					// intial print for entering game
		void shuffle();						// shuffles cards
		int getSize();						// returns size of deck
		double playBlackJack( double );		// BlackJack Interface
		int placeBet( double );				// place a bet at beginning of each game
		void Payout( int );					// payout if win or push

	private:
		void Deal();						// deals cards
		int playAgain();					// asks user if still playing
		int checkWinner();					// determines winner (0 dealer, 1 player, -1 push) & prints appropriate message
		int checkHit();						// returns 0 if stand, 1 if hit
		int checkDoubleDown();				// returns 0 if no, 1 if yes
		int check21( char );				// checks for BlackJack
		void hit( char );					// adds card for hit
		void doubleDown();					// doubles down
		void printHand( int n );			// prints the hand
		void reset();						// resets gameplay

		/* Cards */
		struct Card {
			int suitRank;					// suit rank
			int valueRank;					// value of card
			char suit;						// suit for print
			char value;						// value for print
		};
		deque< Card > deck;					// deque of structs

		/* Player */
		struct Player {
			Card *hand;						// player hand
			int size;						// size of hand for iterating
			int handScore[ 2 ];				// score of hand
			double currentBet;				// bet for that round
			double totalMoney;				// total money
		};	
		Player player;						// create player
		Player dealer;						// create dealer

};

#endif
