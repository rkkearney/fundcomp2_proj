#include "casino.h"
#include "roulette.h"
#include "blackjack.h"
#include "fivecardstud.h"
#include "slots.h"
#include "keno.h"
#include <iomanip>
#include <stdlib.h>
#include <map>

using namespace std;

// function prototype
map<string, double> fileOpen(string file);

int main() {
	// ensure random shuffling
	srand(time(NULL));
	
	// read in username and determine if they've visited the casino before
	string username;
	cout << "Welcome to the Casino! What is your name? ";
	cin >> username;
	int match = 0;
	string line;
	double startMoney = 500;
	map<string, double> inMap = fileOpen("leaderboard.txt");
	for (map<string, double>::iterator it=inMap.begin();it!=inMap.end(); ++it){
		if (it->first == username) {
			startMoney = it->second;
			match = 1;
		}
	}
	if (startMoney == -1) {
		match = 2;
		startMoney = 500;
	}
	
	// instantiate casino object
	casino C(username, startMoney);
	// if the match variable == 1, they have been here before and didn't bankrupt
	if (match == 1) {
		cout << "Good to have you back, " << username << "! It looks like you still have $" << C.getCurrentWallet() << " left." << endl;
	}
	// if match == 2, they bankrupted last time
	else if (match == 2) {
		cout << "Ah, back again " << username << "? Here's another $" << C.getCurrentWallet() << ", try and be more careful with it this time." << endl;
	}
	// if neither of those, they're new to the casino
	else {
		cout << "It looks like you're new to the Casino, " << username << ", so we'll start you off with $" << C.getCurrentWallet() << endl;
	}
	
	// main gameplay 
	C.enterCasino();

	// write out new data to the leaderboard file
	if (C.getCurrentWallet() <= 0) {
		C.setCurrentWallet(-1);
	}
	inMap[username] = C.getCurrentWallet();
	system("rm leaderboard.txt");
	ofstream myfile;
	myfile.open("leaderboard.txt");
	if (myfile.is_open()) {
		for (map<string, double>::iterator it=inMap.begin();it!=inMap.end(); ++it) {
			myfile << it->first << endl;
			myfile << it->second << endl;
		}
	}
	
	// write out the leaderboard to stdout
	map<double, string> outMap;
	int position = 1;
	for (map<string,double>::iterator ii=inMap.begin(); ii!=inMap.end(); ++ii){
		outMap[ii->second] = ii->first;
	}
	cout << "\nCURRENT STANDINGS: " << endl;
	cout << "==================" << endl << endl;
	cout << "\tAmount:\t\tName:" << endl;
	cout << "\t=======\t\t=====" << endl;
 	for (map<double,string>::reverse_iterator jj=outMap.rbegin(); jj!=outMap.rend(); ++jj) {
 		if (jj->first != -1) {
			cout << position << ")\t$" << jj->first << "\t\t" << jj->second << endl;
		}
		else {
			cout << position << ")\t$0" << "\t\t" << jj->second << endl;
		}
		position++;
	}
	cout << endl;
}

// open the file, return a map of unique words and counts
map<string, double> fileOpen(string file) {
	string tmp;
	double tmp2;
	ifstream myFile (file.c_str());
	map<string, double> map1;
	if (myFile.is_open()) {
		while(myFile.good()) {
				myFile >> tmp;
				myFile >> tmp2;
				map1[tmp] = tmp2;
		}
	}
	return map1;
}

