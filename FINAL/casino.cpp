// casino.cpp
// Due 2/8/16
// Author: Taylor Rongaus, Reilly Kearney, Maddie Gleason, and Ronni Sardina
// base class for composition

#include <stdlib.h>
#include "roulette.h"
#include "blackjack.h"
#include "fivecardstud.h"
#include "keno.h"
#include "slots.h"
#include "casino.h"

using namespace std;

// default constructor
casino::casino(string u, double cw) {
	username = u;
	currentWallet = cw;
}

// function to enter the casino and play the games
void casino::enterCasino() {
	int gameChoice = 0;
	int rYes, bYes, fYes, sYes, kYes;

	// call the wordsPrint functions of the base class
	wordsPrint("welcome2.txt");

	// instantiate objects of derived classes
	roulette R;
	blackjack B;
	FiveCard F;
	slots S;
	keno K;
	
	// stay in the casino until user quits or balance < 0
	while (gameChoice != 6) {
		if (getCurrentWallet() > 0) {
			if (gameChoice != 0) {
				wordsPrint("welcome1.txt");
			}
			cout << "You still have $"<< getCurrentWallet() << " in your wallet." << endl;
			gameChoice = print();
		}
		else {
			cout << "You're broke! Get outta here! But come back when you make some more money!" << endl;
			gameChoice = 6;
		}
		cout << endl;
		switch (gameChoice) {
			case 1:
				rYes = 0;
				cout << "Would you like to view the rules? (Y/N) ";
				rYes = yesno();
				if (rYes) {
					wordsPrint("roulette_rules.txt");
				} 
				R.initPrint();
				setCurrentWallet(R.playRoulette(getCurrentWallet()));
			break;
			case 2:
				bYes = 0;
				cout << "Would you like to view the rules? (Y/N) ";
				bYes = yesno();
				if (bYes) {
					wordsPrint("blackjack_rules.txt");
				} 
				B.initPrint();
				setCurrentWallet(B.playBlackJack(getCurrentWallet()));
			break;
			case 3:
				fYes = 0;
				cout << "Would you like to view the rules? (Y/N) ";
				fYes = yesno();
				if (fYes) {
					wordsPrint("fivecardstud_rules.txt");
				} 
				F.initPrint();
				setCurrentWallet(F.playFiveCard(getCurrentWallet()));
			break;
			case 4:
				sYes = 0;
				cout << "Would you like to view the rules? (Y/N) ";
				sYes = yesno();
				if (sYes) {
					wordsPrint("slots_rules.txt");
				} 
				S.initPrint();
				setCurrentWallet(S.playSlots(getCurrentWallet()));
			break;
			case 5:
				kYes = 0;
				cout << "Would you like to view the rules? (Y/N) ";
				kYes = yesno();
				if (kYes) {
					wordsPrint("keno_rules.txt");
				} 
				K.initPrint();
				setCurrentWallet(K.playKeno(getCurrentWallet()));
			break;
			case 6:
				wordsPrint("thanks.txt");
			break;
			default:
				cout << "Error: please enter a valid game choice. " << endl;
			break;
		}
	}

}

// function to determine if user entered a Y/N
int casino::yesno() {
	char c = 'Z';
	if (c != 'Y' || c != 'N' || c != 'y' || c != 'n'){
		cin >> c;
	} 
	if (c == 'Y' || c == 'y') {
		return 1;
	}
	else {
		return 0;
	}
}

// function to read in text files to stdout
void casino::wordsPrint(string file) {
	// print the file to stdout
	string tmp;
	ifstream myFile (file.c_str());
	cout << endl;
	if (myFile.is_open()) {
		while(myFile.good()) {
			getline(myFile, tmp);
			cout << tmp << endl;
		}
	}
}

// menu option print for game choices
int casino::print() {
	int choice;
	cout << "What game would you like to play?" << endl;
	cout << "1) Roulette " << endl;
	cout << "2) Blackjack " << endl;
	cout << "3) Five-Card Stud " << endl;
	cout << "4) Slots " << endl;
	cout << "5) Keno " << endl;
	cout << "6) Quit " << endl;
	cout << "Your choice: ";
	cin >> choice;
	return choice;
}

// get & set functions
string casino::getUsername() {
	return username;
}

void casino::setUsername(string s) {
	username = s;
}

double casino::getCurrentWallet() {
	return currentWallet;
}

void casino::setCurrentWallet(double cw) {
	currentWallet = cw;
}

