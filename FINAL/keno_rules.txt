Keno Rules
===========

Keno is a game in which you place a bet of your choosing and then select 10 numbers ranging from 1 to 80. Ten numbers will then be randomly selected by the house, and you will win a certain payout depending on how many of your numbers match the random ones.

For this particular game, you will be prompted to enter in your ten numbers, separated by pressing the "Enter" key. Once you have entered in your numbers, the winning numbers and the payout table will appear, and you will be informed of your winnings. If you match 1-5 numbers, you will win 3x your bet, 6 will win you 5x your bet, 7 will win you 10x your bet, 8 will win you 50x your bet, 9 will win you 100x your bet, and 10 will win you 500x your bet.

Good luck!
