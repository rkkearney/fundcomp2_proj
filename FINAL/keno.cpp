// keno.cpp
// Due 2/8/16
// Author: Maddie Gleason and Taylor Rongaus
// Derived Class of casino

#include <stdlib.h>
#include "casino.h"
#include "keno.h"

using namespace std;

// non-default constructor
keno::keno() {

}

// function to initialize a random array
void keno::kenoArray(){
	srand((unsigned)time(0));
	for (int i = 0; i < 20; i++){
		gameNumbers[i] = (rand()%80)+1;
	}
}

// ask user for 10 different numbers
void keno::playerChoice(){
	cout << "Please enter your 10 wagered numbers (between 1-80)" << endl;
	int n;
	int maxNums = 10;
	for (int i = 0; i < maxNums; i++){
		if (cin >> n){
			while (n < 1 || n > 80){
				cout << "Invalid number. Try again." << endl;
				cin >> n;
			}
			userNumbers[i] = n;
		}
	}
}

// compare userNumbers to gameNumbers
int keno::checkChoice(){	
	int match = 0;
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 20; j++) {
			if (userNumbers[i] == gameNumbers[j]){
				match +=1;
			}
		}
	}
	sleep(2);
	cout << "Number of matches: " << match << endl;
	sleep(1);
	return match;	
}

double keno::playKeno(double currentMoney){
	int test;
	int playGame = 1, numMatch = 0;
	double bet = 0;
	
	while (playGame == 1){	
		cout << "Your current amount of money: $" << currentMoney << endl;
		cout << "Place your bet: $";
		cin >> bet;
		
		while (bet > currentMoney){
			cout << "Bet too large! Try again!" << endl;
			cout << "Place your bet: ";
			cin >> bet;
		}
		
		currentMoney = currentMoney - bet;

		kenoArray();
		playerChoice();
		cout << endl;
		cout << "---Game Numbers---" << endl; 
		for (int i = 0; i < 20; i++){
			if (i < 19){
				cout << gameNumbers[i] << ", ";
			}
			else {
				cout << gameNumbers[i] << endl;
			}
		}
		cout << endl;
		numMatch = checkChoice();
		// print the payout ratio
		cout << endl;
		cout<<"PAYOUTS:"<<endl 
		  <<"+------------------+"<<endl 
		  <<"| 1 - 5   |    3X  |"<<endl 
		  <<"| 6       |    5X  |"<<endl 
		  <<"| 7       |   10X  |"<<endl 
		  <<"| 8       |   50X  |"<<endl 
		  <<"| 9       |  100X  |"<<endl 
		  <<"| 10      |  500X  |"<<endl 
		  <<"+------------------+"<<endl<<endl; 
		double result;
		if (numMatch == 0) {
				result = 0;
		}
		else if (numMatch >= 1 && numMatch <= 5) {
				result = 3*bet + bet;
		}
		else if (numMatch == 6) {
				result = 5*bet + bet;
		}
		else if (numMatch == 7) {
				result = 10*bet + bet;
		}
		else if (numMatch == 8) {
				result = 50*bet + bet;
		}
		else if (numMatch == 9) {
				result = 100*bet + bet;
		}
		else if (numMatch == 10) {
				result = 500*bet + bet;
		}
		currentMoney = currentMoney + result;
		if (currentMoney > 0) {
			cout << "You now have $" << currentMoney << endl;
			playGame = playAgain();
		}
		else {
			break;
		}
	}
	return currentMoney;
}

int keno::playAgain() {
	char c = 'Z';
	if (c != 'Y' || c != 'N' || c != 'y' || c != 'n'){
		cout << "Would you like to play again? (Y/N) "; 
		cin >> c;
	} 
	if (c == 'Y' || c == 'y') {
		return 1;
	}
	else {
		return 0;
	}
}

void keno::initPrint() {
	// set the name of the file to print
	string file = "keno_word.txt";
	// print the file to stdout
	string tmp;
	ifstream myFile (file.c_str());
	cout << endl;
	if (myFile.is_open()) {
		while(myFile.good()) {
			getline(myFile, tmp);
			cout << tmp << endl;
		}
	}
}



