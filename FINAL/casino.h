// casino.h (class definition)
// Due 2/8/16
// Author: Taylor Rongaus
// Base Class

#ifndef casino_H
#define casino_H
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include "math.h"
#include "roulette.h"
#include "blackjack.h"
#include "fivecardstud.h"
#include "keno.h"
#include "slots.h"
using namespace std;

class casino {

	public:
		casino(string, double);				// default constructor- empty
		void enterCasino();
		int print();									// prints menu to user
		int yesno();
		void wordsPrint(string file);			// prints ascii word art
		string getUsername();					// get function for private username
		void setUsername(string);			// set function for private username
		double getCurrentWallet();			// get function for private currentWallet
		void setCurrentWallet(double);	// set function for private currentWallet
	private:
		string username;						// stores username of current player
		double currentWallet;					// stores their current balance
};

#endif
